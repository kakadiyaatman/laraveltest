<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;

class IPController extends Controller
{
    public function index(Request $request)
    {
        $ip=request()->ip();
        $header= getallheaders();
        $name = $request->get('name');
        return response()->json([
            'Greeting'=> $header['x-hello-world'].", ".$request->name,
            'IP' => $ip
        ], 200);

    }

}
